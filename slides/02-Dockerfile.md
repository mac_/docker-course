### Docker для продолжающих

<small>Created by [Valerii Kravets](viniokil@gmail.com) / [@viniokil](https://t.me/viniokil)</small>

---

![logo](images/docker-official.svg)

## Advanced Dockerfile

---

## Вопросы по домашнему заданию

---

[https://gitlab.com/hexwit/doher/blob/master/Dockerfile](https://gitlab.com/hexwit/doher/blob/master/Dockerfile)

[https://gitlab.com/yaroslav.belinsky/docker-test](https://gitlab.com/yaroslav.belinsky/docker-test)

[https://gitlab.com/biletskyi_dm/dockerfile](https://gitlab.com/biletskyi_dm/dockerfile)

---

## Чем вам полезен Dockerfile?

---

## Почистим весь хлам

```bash
docker container prune
docker image prune
docker volume prune
docker network prune

```

---

```bash
docker system prune
```

```bash
WARNING! This will remove:
        - all stopped containers
        - all networks not used by at least one container
        - all dangling images
        - all build cache
Are you sure you want to continue? [y/N] y
```

---

## Использование диска

```bash
docker system df
```

---

# Облегчение приложения

---

## Приложение на go

---

```golang
package main
import (
  "net/http"
  "strings"
)
func sayHello(w http.ResponseWriter, r *http.Request) {
  message := r.URL.Path
  message = strings.TrimPrefix(message, "/")
  message = "Hello " + message
  w.Write([]byte(message))
}
func main() {
  http.HandleFunc("/", sayHello)
  if err := http.ListenAndServe(":80", nil); err != nil {
    panic(err)
  }
}

```

---

## Называй меня зайка, называй меня...

## hello.go

---

```Dockerfile
FROM golang:latest

WORKDIR /app

COPY hello.go hello.go

ENTRYPOINT go run hello go
```

---

## Сборка приложения

```bash
docker build -t hello-go:v1 .
```

---
## Информация о размере

```bash
docker image inspect hello-go:v1 --format='{{.Size}}' \
    | numfmt --to=iec
```

### 779M

---

<section data-background-video="images/nope.mp4" data-background-video-loop data-background-video-muted>
</section>

---

## Окей

~~FROM golang:latest~~

```Dockerfile
FROM golang:alpine
```

```bash
docker build -t hello-go:v2 --file Dockerfile.2 .
```

---

## А теперь

```bash
docker image inspect hello-go:v2 --format='{{.Size}}' \
    | numfmt --to=iec
```

### 296M

---

### Что не так?

---

~~FROM golang:alpine~~

```Dockerfile
FROM golang:alpine AS builder
```

---

```Dockerfile
FROM golang:alpine AS builder

WORKDIR /app

COPY hello.go hello.go 
```

---

```Dockerfile
FROM golang:alpine AS builder

WORKDIR /app

COPY hello.go hello.go
# Build the binary.
RUN go build -o /app/hello
```

---

### Берем пустой образ и копируем скомпилированый файл

```Dockerfile
FROM scratch

COPY --from=builder /app/hello /app/hello
```

---

### Запускаем новое приложение

```Dockerfile
ENTRYPOINT ["/app/hello"]
```
---

### Итого

```Dockerfile
FROM golang:alpine AS builder

WORKDIR /app

COPY hello.go hello.go 

RUN go build -o /app/hello

FROM scratch

COPY --from=builder /app/hello /app/hello

ENTRYPOINT ["/app/hello"]
```

---

```bash
docker build -t hello-go:v3 --file Dockerfile.3 .
```

---

```bash
docker image inspect hello-go:v3 --format='{{.Size}}' \
    | numfmt --to=iec
```

---

### Сколько?

---

# 6.3M

---

## Классно?

---

```bash
 docker run hello-go:v3
```

```bash
standard_init_linux.go:207: exec user process caused "no such file or directory"
```

---

# ?WTF

---

```Dockerfile
RUN CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64 \
    go build -a -tags netgo -ldflags '-w' -o /app/hello
```

---

```bash
docker run --rm -p 80:80 hello-go:v3
```

[http://localhost/i_am_be_back](http://localhost/i_am_be_back)

---

```bash
docker image inspect hello-go:v3 --format='{{.Size}}' | numfmt --to=iec
```

### 5.1M

---

| golang:latest | golang:alpine | multi-stage build |
|---------------|---------------|-------------------|
| 779M          | 296M          | 5.1M              |

---

# ДЗ

---

- Максимально облегчить ваши проекты
- Коментировать каждую строчку
- Придумать выпускной проект

--- 

- Зарегистрироваться на [Docker HUB](http://hub.docker.com)
- Создать в нем репозиторий проекта
- Написать описание проекта и его запуска
- Запушить готовый docker image на Docker HUB

---

Александр

# https://github.com/yugandhargangu/JspMyAdmin2

---

### Облегчение проектов

- очистка кешей менеджеров пакетов
- удаление зависимостей сборки
- мульти этапная сборка

---

![vse-sterpit](images/vse-sterpit.jpg)