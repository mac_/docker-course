### Docker для продолжающих

<small>Created by [Valerii Kravets](viniokil@gmail.com) / [@viniokil](https://t.me/viniokil)</small>

---

![logo](images/docker-official.svg)

## Docker-compose

---

## Вопросы по домашнему заданию

---

https://hub.docker.com/r/yaroslavbelinsky/ionic-go
https://gitlab.com/yaroslav.belinsky/ionic-docker

https://hub.docker.com/r/biletskiydm/nodeadmin

https://hub.docker.com/r/hexwit/jsp-myadmin
https://gitlab.com/hexwit/doher/blob/master/lesson-2-homework/Dockerfile
---

## Проблемы

- Одно приложение состоит из множества контейнеров/сервисов
- Один контейнер зависит от другого
- Порядок запуска имеет значение
- docker build/run/create ... (долго и много)

---

## Docker-compose
- Отдельная утилита
- Декларативное описание docker-инфраструктуры в YAML-формате
- Управление многоконтейнерными приложениями


---

### MySQL

```shell
docker run -v "$PWD/data":/var/lib/mysql \
          --user 1000:1000 \
          --name some-mysql \
          -e MYSQL_ROOT_PASSWORD=my-secret-pw \
          -d mysql:5.7
```

---

### Запуск от имени пользователя

```shell
export $UID
export $GID
```

---

### Создание базы. Переменная MYSQL_DATABASE

```shell
docker run -v "$PWD/data":/var/lib/mysql \
          --user $UID:$GID \
          --name some-mysql \
          -e MYSQL_ROOT_PASSWORD=my-secret-pw \
          -e MYSQL_DATABASE=test_db \
          -d mysql:5.7
```

---

### Пробросим порты

```shell
docker run -p 3306:3306 \
          -v "$PWD/data":/var/lib/mysql \
          --user $UID:$GID \
          --name some-mysql \
          -e MYSQL_ROOT_PASSWORD=my-secret-pw \
          -e MYSQL_DATABASE=test_db \
          -d mysql:5.7
```

---

![slojna](images/slojna.jpg)

---

# docker-compose.yml

---

### Обязательно. Один раз в начале

```yml
version: '3' # Версия docker-compose.yml

services: # Обьявляем начало описания сервисов
```

---

```yml
services: 

  db: # имя сервиса. 
      # аналог docker run --name db
    image: mysql # название сервиса, без тега - latest
    environment: # начало списка переменных окружения
      MYSQL_ROOT_PASSWORD: example # специфичная переменная
                                   # пароль пользователя root
```

---

```yml
services:

  db:
    image: mysql
    environment:
      MYSQL_ROOT_PASSWORD: example
    ports: # описание пробрасываемых портов
    - 3306:3306 # пробрасываем порт локальный:контейнер
```
---

### Поднимаем докер контейнеры
```shell
docker-compose up 
```

### Режим demonize
```shell
docker-compose up -d
```

---

### Cтучимся в MySQL in Docker
```shell
mysql -h 127.0.0.1 -P 3306 -u root -p example
```

### Список баз MySQL
```sql
SHOW DATABASES;
exit;
```

---

```yml
  db:
    image: mysql
    restart: always # перезапускать всегда
                    # один раз на сервис
    environment: # один раз. Список из 1> элеменов
      MYSQL_ROOT_PASSWORD: example
```

---

### Варианты 

```yml
restart: "no" # пускай падает. По умолчанию
restart: always # всегда
restart: on-failure # при ошибке во время работы
restart: unless-stopped # пока не остановят
```
---

```yml
services:
  db:
    ...
  adminer:
    image: adminer
    restart: always
    ports:
      - 8080:8080
```

### cтартуем 
```shell
docker-compose up -d
```

---

### смотрим рабочие процессы

```shell
docker-compose ps
```

### смотрим логи

```shell
docker-compose logs
```

---

**[http://localhost:8080](http://localhost:8080)**

логин: **root**

пароль: **example**

---

### Прописываем переменные окружения для Wordpress

```yml
version: '3'

services:
  wordpress:
    image: wordpress
    restart: always
    ports:
      - 8888:80 # пробрасываем на локальный порт 8888 
    environment:
      WORDPRESS_DB_HOST: db # совпадает с названием сервиса c БД
      WORDPRESS_DB_USER: exampleuser # совпадает с  MYSQL_USER
      WORDPRESS_DB_PASSWORD: examplepass # MYSQL_PASSWORD
      WORDPRESS_DB_NAME: exampledb # MYSQL_DATABASE
```

---

### Прописываем переменные окружения для БД

```yml
services:
  wordpress:
    ...
  adminer:
    ...
  db:
    ...
    environment:
      MYSQL_DATABASE: exampledb # == WORDPRESS_DB_NAME
      MYSQL_USER: exampleuser   # == WORDPRESS_DB_USER
      MYSQL_PASSWORD: examplepass # == WORDPRESS_DB_PASSWORD
      MYSQL_RANDOM_ROOT_PASSWORD: '1' # рандомный рут пароль
```

---

**[http://localhost:8888](http://localhost:8888)**

---

### Переменные в yaml

```yml
&DB_HOST db: # задать переменную DB_HOST со значение 'db'
             # при этом db также является названием сервиса
  environment:
  ...
wordpress:
  ...
  environment:
  WORDPRESS_DB_HOST: *DB_HOST # задать переменную WORDPRESS_DB_HOST
                              # со значением из переменной DB_HOST
```
```yml
WORDPRESS_DB_HOST: *DB_HOST
```
**==**
```yml
WORDPRESS_DB_HOST: db
```

---
```yml
&DB_HOST db: # Вносим в переменную DB_HOST имя хоста 'db'
  ...
  environment:
    MYSQL_USER: &DB_USER exampleuser # задаем переменную DB_USER
    MYSQL_PASSWORD: &DB_PASSWORD examplepass # --//--
    MYSQL_DATABASE: &DB_NAME exampledb # == WORDPRESS_DB_NAME
    MYSQL_RANDOM_ROOT_PASSWORD: '1' # рандомный рут пароль
wordpress:
  ...
  environment:
    WORDPRESS_DB_HOST: *DB_HOST # совпадает с названием сервиса c БД
    WORDPRESS_DB_USER: *DB_USER # совпадает с  MYSQL_USER
    WORDPRESS_DB_PASSWORD: *DB_PASSWORD # MYSQL_PASSWORD
    WORDPRESS_DB_NAME: *DB_NAME # MYSQL_DATABASE
```

---
```yml
version: '3'

services:

  &DB_HOST db:
    image: mysql:5.7
    restart: always
    environment:
      MYSQL_USER: &DB_USER exampleuser # задаем переменную DB_USER
      MYSQL_PASSWORD: &DB_PASSWORD examplepass # --//--
      MYSQL_DATABASE: &DB_NAME exampledb # == WORDPRESS_DB_NAME
      MYSQL_RANDOM_ROOT_PASSWORD: '1' # рандомный рут пароль

  wordpress:
    image: wordpress
    restart: always
    ports:
      - 8888:80
    environment:
      WORDPRESS_DB_HOST: *DB_HOST # совпадает с названием сервиса c БД
      WORDPRESS_DB_USER: *DB_USER # совпадает с  MYSQL_USER
      WORDPRESS_DB_PASSWORD: *DB_PASSWORD # MYSQL_PASSWORD
      WORDPRESS_DB_NAME: *DB_NAME # MYSQL_DATABASE

  adminer:
    image: adminer
    restart: always
    ports:
      - 8080:8080
```
---

### Проверяем

```shell
docker-compose down
```
```shell
docker-compose up -d
```
```shell
docker-compose ps
```
---

**[http://localhost:8888](http://localhost:8888)**

```shell
docker-compose down -v # -v удалить Volume's
```

---

# ДЗ

---

### Написать docker-compose.yml

- {{ you-admin}} + Postgres
- Rocket chat (nodeJS + mongoDB)
- MongoDB + mongoclient + mongo-express
- Postgres + pgAdmin
- Adminer + mariaDB + Postgres + MongoDB

---

![win.jpeg](images/win.jpeg)