### Docker для продолжающих

<small>Created by [Valerii Kravets](viniokil@gmail.com) / [@viniokil](https://t.me/viniokil)</small>

---

![logo](images/docker-official.svg)

## Docker-compose

---

## Вопросы по домашнему заданию

---

## Алексей

---

### Rocket chat (nodeJS + MongoDB)

```yaml
  mongo:
    image: mongo:3.2
    restart: unless-stopped
    volumes:
     - ./data/db:/data/db
```

---

### Rocket chat (nodeJS + MongoDB)
   
* Нужно починить запуск приложения
* Не запускается через docker-compose up -d
* Переписать на docker-compose v3

---

## Стас

---

### Postgres + pgAdmin

```yaml
version: '3'

services:
  postgres:
    container_name: postgres_container
    image: postgres:latest
    hostname: postgres
    environment:
      POSTGRES_USER: postgres
      POSTGRES_PASSWORD: 1111
      POSTGRES_DATABASE: postgres
    volumes:
    - postgres:/data/postgres
    ports:
    - "8080:5432"
```

---

### Postgres + pgAdmin


* **D**ocker-compose.yml файл давал странный аффект
* Запускались сторонние сервисы
* Использовать только **docker-compose.yml**

---

### Как дебажил

```shell
docker-compose ps --services

postgres
redis
queue
php-fpm
nginx
app
scheduler
```

---

### Смотрим сгенерированный конфиг с переменными

```shell
docker-compose config
```

### Смотрим сгенерированный сервисы из docker-compose.yml

```shell
docker-compose ps --services
postgres
pgadmin
```

---

## Вова

---

### ModX

```yaml
# версия докер-композа
version: '3'

# список используемых сервисов
services:
  # Сервис 1 база данных
  &DB_HOST mysql:
    # образ стандартный
    image: mysql:5.7
```

---

### ModX

```shell
-rwxrwxr-x 1 vinz vinz  599 лют  7 01:25 docker-compose.yml
```

* 777 права. Зачем?
* Как можно автоматизировать git clone?

---

```yml
version: '3'

services:
  modx:
    # че париться? берем готовый образ
    image: chialab/php:7.2-apache
    # называем контейнер не по-кугутски
    container_name: modx
    command: git -C /var/www/html clone https://github.com/evolution-cms/evolution.git
    working_dir: /var/www/html
    # монтируем папку с модх с гита в корень папки апача
    volumes:
    - ./evolution:/var/www/html
```

---

### Определяем что выполнить, при запуске контейнера

```yml
command: git -C /var/www/html clone https://github.com/evolution-cms/evolution.git
```

---

### Что дают эти переменные для ModX?
```yml
    # транслируем переменные в $_ENV надо в конфиге не забыть
    environment:
      DB_HOST: *DB_HOST
      DB_USER: *DB_USER
      DB_PASSWORD: *DB_PASS
      DB_NAME: *DB_BASE
```

---

### Без вдруг

```yml
    # бросаем порты, а вдруг Валерка научит ssl прикручивать?
    ports:
    - 80:80
    - 443:443
```

---

## Богдан

---

### MongoDB + mongoclient + mongo-express

```yml
version: '3'

services:
    &MONGO_CONTAINER mongodb:
      image: mongo:latest
      container_name: mongodb
      ports:
        - 27017:27017
      volumes:
        - ./data/db:/data/db
      environment:
        MONGODB_USER: &MONGODB_USER "user" 
        MONGODB_PASS: &MONGODB_PASS "pass"
```

---

### MongoDB + mongoclient + mongo-express

* Нeгодяй =)
* Какая из админок понравилась?

---

![lets_start](images/lets_start.png)

---

## Осторожно - много букв

---

```yml
version: '3'

services:

  &DB_HOST db:
    image: mysql:5.7
    restart: always
    environment:
      MYSQL_USER: &DB_USER exampleuser # задаем переменную DB_USER
      MYSQL_PASSWORD: &DB_PASSWORD examplepass # --//--
      MYSQL_DATABASE: &DB_NAME exampledb # == WORDPRESS_DB_NAME
      MYSQL_RANDOM_ROOT_PASSWORD: '1' # рандомный рут пароль

  wordpress:
    image: wordpress
    restart: always
    ports:
      - 8888:80
    environment:
      WORDPRESS_DB_HOST: *DB_HOST # совпадает с названием сервиса c БД
      WORDPRESS_DB_USER: *DB_USER # совпадает с  MYSQL_USER
      WORDPRESS_DB_PASSWORD: *DB_PASSWORD # MYSQL_PASSWORD
      WORDPRESS_DB_NAME: *DB_NAME # MYSQL_DATABASE

  adminer:
    image: adminer
    restart: always
    ports:
      - 8080:8080
```

---

### Как все потерять с таким конфигом? 

```shell
docker-compose down -v
```

---

### Что хранить?

* Данные приложения
* Базу данных

---

```yml
  &DB_HOST db:
    image: mysql:5.7
    restart: always
    environment:
      MYSQL_USER: &DB_USER exampleuser # задаем переменную DB_USER
      MYSQL_PASSWORD: &DB_PASSWORD examplepass # --//--
      MYSQL_DATABASE: &DB_NAME exampledb # == WORDPRESS_DB_NAME
      MYSQL_RANDOM_ROOT_PASSWORD: '1' # рандомный рут пароль
    volumes:
      - ./mysql:/var/lib/mysql # Монтируем папку контейнера /var/lib/mysql
                               # в локальную папку по пути ./mysql
```

---

```yml
  wordpress:
    image: wordpress
    restart: always
    ports:
      - 8888:80
    environment:
      WORDPRESS_DB_HOST: *DB_HOST # совпадает с названием сервиса c БД
      WORDPRESS_DB_USER: *DB_USER # совпадает с  MYSQL_USER
      WORDPRESS_DB_PASSWORD: *DB_PASSWORD # MYSQL_PASSWORD
      WORDPRESS_DB_NAME: *DB_NAME # MYSQL_DATABASE
    volumes:
      - ./wordpress:/var/www/html # Монтируем папку контейнера /var/www/html
                                  # в локальную папку по пути ./wordpress

```

---

### Сохраним (но не сильно) папку home
```yml
version: '3'

volumes: # Обьявляем о существовании томов
  home-dir: # Название вольюма

services:
  wordpress:
    image: wordpress
    ...
    volumes:
      - ./wordpress:/var/www/html
      - home-dir:/root # Монтируем папку контейнера /root
                       # в локальную docker volume home-dir
```

---

### Монтирование только для чтения 

```yml
    volumes:
      - ./wordpress:/var/www/html:ro
```

### Явно разрешить запись

```yml
    volumes:
      - ./wordpress:/var/www/html:rw
```

---

### Где будет храниться остальное?

### в UnionFS

---

---

### Добавим общую сеть 

По умаолчанию сеть default

```yml
version: '3'

# Блок networks в начале или конце файла
networks: # Начало описание сетей
  backend-network: # Название сети
    driver: bridge # Тип сети

services:
  &DB_HOST db:
    image: mysql:5.7
    ...
    networks: # Контейнер подключить к сети
      - backend-network # с названием `backend-network`
  wordpress:
    image: wordpress
    ...
    ports:
      - 8888:80
    networks:
      - backend-network
  adminer:
    image: adminer
    ...
    ports:
      - 8080:8080
    networks:
      - backend-network
```

---

### docker-compose поддерживает .env

```yml
services:

  &DB_HOST db:
    image: mysql:5.7
    restart: always
    environment:
      MYSQL_USER: &DB_USER exampleuser # задаем переменную DB_USER
      MYSQL_PASSWORD: &DB_PASSWORD examplepass # --//--
      MYSQL_DATABASE: &DB_NAME exampledb # == WORDPRESS_DB_NAME
      MYSQL_RANDOM_ROOT_PASSWORD: '1' # рандомный рут пароль
    env_file:
      - .env
```


### Содержимое .env

```conf
DB_USER=exampleuser
DB_PASSWORD=examplepass
DB_NAME=exampledb
```

```yml
services:

  &DB_HOST db:
    image: mysql:5.7
    restart: always
    environment:
      MYSQL_USER: &DB_USER ${DB_USER} # задаем переменную DB_USER
      MYSQL_PASSWORD: &DB_PASSWORD ${DB_PASSWORD} # --//--
      MYSQL_DATABASE: &DB_NAME ${DB_PASSWORD} # == WORDPRESS_DB_NAME
      MYSQL_RANDOM_ROOT_PASSWORD: '1' # рандомный рут пароль
    env_file:
      - .env
```

---

### Добавляйте .env в .gitignore !!!

### Создайте .env.example

---

### Опции описания сетей

```yml
networks: # Начало описание сетей
  1-network: # Название сети
    driver: bridge # Тип сети
  2-network: # Название сети
    external: # Сеть существует
```

Создать сеть

```shell
  docker network create 2-network
```

---

# ДЗ

---

* Зарегестрироваться на hub.docker.com
* Создать приложение mysql
* Изменить my.cnf (конфиг MySQL)
* Написать Dockerfile
* Запушить в Docker hub
* Написать docker-compose.yml, использующий Dockerfile
* Добавить веб-админку
* Сбилдить и запушить используя docker-compose

---

### MySQL с правильной кодировкой utf8_general_ci

```conf
character-set-server=utf8
collation-server=utf8_general_ci
```

(stackoverflow.com)[https://stackoverflow.com/questions/46004648/how-to-setup-mysql-with-utf-8-using-docker-compose]

---

### Кастомим как Батя

```yml
version: '3'
services:
  webapp:
    build: # Говорим сбилдить имедж
      context: ./dir # Контекст сборки
      dockerfile: Dockerfile-alternate # Особое имя Dockerfile
      args: # аргументы для docker build
        buildno: 1 # Номер билда
    image: vinz/mysql:latest # обьявляем имя имеджа
```

---
### Применяемыe команды

```shell
docker-compose pull # скачать все имеджи для сервисов

docker-compose build # собрать имеджи для сервисов

docker-compose build up -d --build # Пересобрать и поднять сервисы

docker-compose build up -d --no-build # Поднять сервисы без перезборки

docker-compose push # отправить собранные контейнеры на docker hub

```

---

![win.jpeg](images/win.jpeg)